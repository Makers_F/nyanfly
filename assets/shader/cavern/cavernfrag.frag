/*upperFragmentShader*/
precision mediump float;

/* externally defined:
	QUALITY_CODE
	SCREEN_WIDTH
	SCREEN_HEIGHT
 */

/* the mapping bust be done one the whole texture, not only the atlas used by the mesh, otherwise there will be
 * distortion.
 */
#if QUALITY_CODE == 3 /* high */
const vec2 TEXTURE_DIMENSION = vec2(768.0, 682.0);
#elif QUALITY_CODE == 2 /* medium */
const vec2 TEXTURE_DIMENSION = vec2(512.0, 512.0);
#else /* low */
const vec2 TEXTURE_DIMENSION = vec2(341.0, 342.0);
#endif

uniform sampler2D u_texture_0;
uniform vec4 u_textureBounds;
uniform float u_alignedOffset;

varying vec2 v_position;

void main(void)
{
	vec2 adjusted_position = v_position + vec2(u_alignedOffset, 0.0);
	float sampling_coords_U = u_textureBounds.x + mod(adjusted_position.x / TEXTURE_DIMENSION.x, u_textureBounds.z);
	float sampling_coords_V = u_textureBounds.y + mod(1.0 - (adjusted_position.y / TEXTURE_DIMENSION.y), u_textureBounds.w);
	vec4 tex_color = texture2D(u_texture_0, vec2(sampling_coords_U, sampling_coords_V));
	gl_FragColor = tex_color;
}