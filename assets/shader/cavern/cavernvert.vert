/* upperVertexShader*/
/* externally defined:
 *	QUALITY_CODE
 * 	SCREEN_WIDTH
 *	SCREEN_HEIGHT
*/
/*
 * T step (float edge , T x ) 0.0 if x < edge , else 1.0
 */
#define DISTANCE(a,b) abs(a - b)
uniform mat4 u_modelViewProjectionMatrix;
uniform float u_unalignedOffset;
uniform mat4 u_specialCoordinatesPosition;
/*
* u_specialCoordinatesPosition = {
*	{x1, x2, x3, x4},
*	{y1, y2, y3, y4},
*	{x5, ...},
*	{}}
*/

attribute vec4 a_position;

varying vec2 v_position;

const float ERROR_LIMIT = 0.000001;
const float MIN_VALUE = 2e-10;
const float MAX_VALUE = 2e10;

float scurve3(float a) {
	float c = clamp(a, 0.0, 1.0);
	return (c * c * (3.0 - 2.0 * c));
}

float scurve5(float a) {
	float c = clamp(a, 0.0, 1.0);
	float c3 = c*c*c;
	float c4 = c3 * c;
	float c5 = c4 * c;
	return (6.0 * c5) - (15.0 * c4) + (10.0 * c3);
}

float weight(float a, float minV, float maxV) {
	return clamp((a - minV ) / (maxV - minV + MIN_VALUE), 0.0, 1.0);
}

float min4fv(vec4 vector) {
	return min(min(vector.x, vector.y),min(vector.z, vector.w));
}

float max4fv(vec4 vector) {
	return max(max(vector.x, vector.y),max(vector.z, vector.w));
}

/* only return the correct value to offset the vertex if the vertex is one of the ones in the uniform, 0.f otherwise*/
float getValueFromXPosition(float position) {
	float value;
	vec4 filter;
	vec4 positionX = vec4(position);

	// first row
	// 1.0 if positionX is almost equal to the one specified in the uniform
	filter = vec4(1.0) - step(ERROR_LIMIT, DISTANCE(u_specialCoordinatesPosition[0], positionX));
	filter = filter * u_specialCoordinatesPosition[1];
	value = filter.x + filter.y + filter.z + filter.w;

	// second row
	filter = vec4(1.0) - step(ERROR_LIMIT, DISTANCE(u_specialCoordinatesPosition[2], positionX));
	filter = filter * u_specialCoordinatesPosition[3];
	value += filter.x + filter.y + filter.z + filter.w;

	return value;
}

vec2 getBoundingVerticesXComponent(float position) {
	vec4 pos_vec = vec4(position);
	/* .x = left vertex
	 * .y = right vertex
	 */
	vec2 bounding_vertices;
	// point.x < position => 0.0
	// point.x > position => 1.0
	vec4 filter_L;
	// point.x < position => 1.0
	// point.x > position => 0.0
	vec4 filter_R;
	vec4 filtered_vertieces;
	vec4 special_coords;

	// first row
	special_coords = u_specialCoordinatesPosition[0];
	// search for the X value in special_coords that is at left and nearest to position
	filter_L = step(position + ERROR_LIMIT, special_coords);
	filtered_vertieces = special_coords - filter_L * MAX_VALUE;
	bounding_vertices.x = max4fv(filtered_vertieces);

	// search for the X value in special_coords that is at right and nearest to position
	filter_R = vec4(1.0) - step(position - ERROR_LIMIT, special_coords);
	filtered_vertieces = special_coords + filter_R * MAX_VALUE;
	bounding_vertices.y = min4fv(filtered_vertieces);

	// second row
	special_coords = u_specialCoordinatesPosition[2];
	filter_L = step(position + ERROR_LIMIT, special_coords);
	filtered_vertieces = special_coords - filter_L * MAX_VALUE;
	bounding_vertices.x = max(bounding_vertices.x, max4fv(filtered_vertieces));

	filter_R = vec4(1.0) - step(position - ERROR_LIMIT, special_coords);
	filtered_vertieces = special_coords + filter_R * MAX_VALUE;
	bounding_vertices.y = min(bounding_vertices.y, min4fv(filtered_vertieces));

	return bounding_vertices;
}

float getInterpYOffsetFromVertexBound(vec2 bounds_x_componet, float position) {
	float position_L = getValueFromXPosition(bounds_x_componet.x);
	float position_R = getValueFromXPosition(bounds_x_componet.y);
	float w = weight(position, bounds_x_componet.x, bounds_x_componet.y);
	float a = scurve3(w); // Or sc5?
	return mix(position_L, position_R, a);
}

void main(void) {
	vec2 coordinate_filter = vec2(1.0, step(ERROR_LIMIT, abs(a_position.y)));
	vec2 boundV = getBoundingVerticesXComponent(a_position.x);
	vec2 pos = vec2(a_position.x, getInterpYOffsetFromVertexBound(boundV, a_position.x)) * coordinate_filter;

	v_position = pos;
	vec2 unaligned_offset_corrected_position = pos - vec2(u_unalignedOffset, 0.0);

	gl_Position  = u_modelViewProjectionMatrix * vec4(unaligned_offset_corrected_position, 0.0, 1.0);
}