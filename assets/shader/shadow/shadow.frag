precision highp float;

varying vec4 v_color;
uniform mat4 u_shadow_lights_x;
uniform mat4 u_shadow_lights_y;
uniform mat4 u_shadow_lights_d;
uniform float u_shadow_transition_percent;
#define MAX_I 4
#define MAX_J 4
#define TRANSPARENT 0.0
#define SOLID 1.0

float getAlphaFromLight(float center_x, float center_y, float d, float k) {
	vec2 coord = gl_FragCoord.xy;
	float x = distance(vec2(center_x, center_y), coord);
	float neg_alpha = clamp(-(x-d)/(k*d) + 1.0, 0.0, 1.0);
	return 1.0 - neg_alpha;
	/*if(x <= d)
		return TRANSPARENT;
	else if (x >= (1.0 + k) * d)
		return SOLID;
	else {
		float weight = smoothstep(d, (1.0 + k) * d, x);
		return mix(TRANSPARENT, SOLID, weight);
	}*/
}

float getTotalAlpha() {
	float alpha = 1.0;
	for(int i = 0; i < MAX_I; i++) {
		for(int j = 0; j < MAX_J; j++) {
			alpha *= getAlphaFromLight(u_shadow_lights_x[i][j], u_shadow_lights_y[i][j], u_shadow_lights_d[i][j], u_shadow_transition_percent);
		}
	}
	return alpha;
}
void main() {
	gl_FragColor = vec4(v_color.xyz, getTotalAlpha());
}