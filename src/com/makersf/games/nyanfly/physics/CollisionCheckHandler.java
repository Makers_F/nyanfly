package com.makersf.games.nyanfly.physics;

import java.util.EnumMap;
import java.util.LinkedList;
import java.util.Map;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.sprite.Sprite;

import com.makersf.andengine.extension.collisions.bindings.ShapeAdapter;
import com.makersf.andengine.extension.collisions.entity.sprite.PixelPerfectSprite;
import com.makersf.frameworks.shared.collisioncore.pixelperfect.IShape;
import com.makersf.frameworks.shared.collisioncore.pixelperfect.PixelPerfectCollisionChecker;
import com.makersf.games.nyanfly.assets.GRAPHIC_QUALITY;
import com.makersf.games.nyanfly.cavern.CavernProvider;
import com.makersf.games.nyanfly.physics.ICollisionCallback.ColliderType;

public class CollisionCheckHandler implements IUpdateHandler {

	private static final int NUMBER_OF_SAMPLES = 10;
	private static final float[] cached_array = new float[2*NUMBER_OF_SAMPLES];

	private final PixelPerfectSprite mSprite;
	private final IShape mShape;
	private final GRAPHIC_QUALITY mQuality;
	private Map<ColliderType, LinkedList<ICollisionCallback>> mCollisionCallbacks = new EnumMap<ICollisionCallback.ColliderType, LinkedList<ICollisionCallback>>(ColliderType.class);
	private Map<ColliderType, LinkedList<Sprite>> mColliders = new EnumMap<ICollisionCallback.ColliderType, LinkedList<Sprite>>(ColliderType.class);
	private CavernProvider mCavern;

	public CollisionCheckHandler(PixelPerfectSprite pSprite, GRAPHIC_QUALITY pQuality) {
		mSprite = pSprite;
		mShape = new ShapeAdapter(pSprite);
		mQuality = pQuality;
		for(ColliderType c : ColliderType.values()) {
			mCollisionCallbacks.put(c, new LinkedList<ICollisionCallback>());
		}
		for(ColliderType c : ColliderType.values()) {
			mColliders.put(c, new LinkedList<Sprite>());
		}
	}

	public void registerCollisionCallback(ColliderType pColliderType, ICollisionCallback pCollisionCallback) {
		mCollisionCallbacks.get(pColliderType).add(pCollisionCallback);
	}

	public boolean unregisterCollisionCallback(ColliderType pColliderType, ICollisionCallback pCollisionCallback) {
		return mCollisionCallbacks.get(pColliderType).remove(pCollisionCallback);
	}

	public void setCavern(CavernProvider pCavern) {
		mCavern = pCavern;
	}

	public void addCollider(ColliderType pType, Sprite pSprite) {
		mColliders.get(pType).add(pSprite);
	}

	public boolean removeCollider(ColliderType pType, Sprite pSprite) {
		return mColliders.get(pType).remove(pSprite);
	}

	private void notifyCollision(ColliderType pType, Object pObject) {
		for(ICollisionCallback c : mCollisionCallbacks.get(pType)) {
			c.onCollisionDetected(pType, pObject);
		}
	}

	@Override
	public void onUpdate(float pSecondsElapsed) {
		if(mCavern != null) {
			if(checkCavern(true)) {
				notifyCollision(ColliderType.Cavern, Boolean.TRUE);
			}
			if(checkCavern(false)) {
				notifyCollision(ColliderType.Cavern, Boolean.FALSE);
			}
		}
		for(ColliderType type : ColliderType.values()) {
			for(Sprite sprite : mColliders.get(type)) {
				if(mSprite.isVisible() && mSprite.collidesWith(sprite)) {
					notifyCollision(type, sprite);
				}
			}
		}
	}

	@Override
	public void reset() {
	}

	private boolean checkCavern(boolean pIsUpper) {
		float width = mSprite.getWidth() * mSprite.getScaleX();
		float offset = mSprite.getX() - width/2;
		float step = width / NUMBER_OF_SAMPLES - 1;
		for(int i=0; i < NUMBER_OF_SAMPLES; i++) {
			cached_array[2*i] = offset + i*step;
			cached_array[2*i + 1] = mQuality.HEIGHT * mCavern.sample(offset + i*step, pIsUpper, mQuality.WIDTH);
		}
		int collidingIndex = PixelPerfectCollisionChecker.checkCollsion(mShape, mSprite.getPixelPerfectMask(), cached_array);
		return collidingIndex != PixelPerfectCollisionChecker.NO_COLLISION_FOUND;
	}
}
