package com.makersf.games.nyanfly.physics;

public interface ICollisionCallback {

	public void onCollisionDetected(ColliderType pType, Object pColeedee);

	public static enum ColliderType {
		Cavern,
		Light;
	}
}
