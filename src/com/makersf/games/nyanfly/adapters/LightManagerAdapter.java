package com.makersf.games.nyanfly.adapters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.makersf.andengine.extension.collisions.entity.sprite.PixelPerfectSprite;
import com.makersf.andengine.extension.collisions.opengl.texture.region.PixelPerfectTextureRegion;
import com.makersf.games.nyanfly.assets.GRAPHIC_QUALITY;
import com.makersf.games.nyanfly.perks.Light;
import com.makersf.games.nyanfly.perks.LightManager;
import com.makersf.games.nyanfly.physics.ICollisionCallback;

public class LightManagerAdapter implements IUpdateHandler, ICollisionCallback {

	private static final int OUT_OF_SCREEN = -300;

	private final LightManager mLightManager;
	private final List<PixelPerfectSprite> mSprites = new ArrayList<PixelPerfectSprite>();
	private final GRAPHIC_QUALITY mQuality;
	private final Queue<Runnable> mOnNextUpdateOperations = new LinkedList<Runnable>();

	public LightManagerAdapter(LightManager pLightManager, PixelPerfectTextureRegion pLightSprite, GRAPHIC_QUALITY pQuality, VertexBufferObjectManager pVBO) {
		mLightManager = pLightManager;
		mQuality = pQuality;
		for(int i = 0; i < mLightManager.getMaxLights(); i++) {
			PixelPerfectSprite sprite = new PixelPerfectSprite(OUT_OF_SCREEN, OUT_OF_SCREEN, pLightSprite, pVBO);
			mSprites.add(sprite);
			setUnvisible(sprite);
		}
	}

	public List<PixelPerfectSprite> getLightsSprites() {
		return Collections.unmodifiableList(mSprites);
	}

	@Override
	public void onUpdate(float pSecondsElapsed) {
		// needed to remove the lights on the next update, so that when a collision happens other classes (like LightComponet) can still access it
		while(!mOnNextUpdateOperations.isEmpty()) {
			mOnNextUpdateOperations.poll().run();
		}
		mLightManager.onUpdate(pSecondsElapsed);
		for(Sprite sprite : mSprites) {
			setUnvisible(sprite);
		}
		int index = 0;
		List<Light> lights = mLightManager.getLightInformations();
		for(Light light : lights) {
			Sprite sprite = mSprites.get(index);
			setVisible(sprite, light);
			index++;
		}
	}

	@Override
	public void reset() {
		mLightManager.reset();
	}

	private void setVisible(Sprite pSprite, Light pLight) {
		pSprite.setPosition(pLight.x * mQuality.WIDTH, pLight.y * mQuality.HEIGHT);
		pSprite.setVisible(true);
		pSprite.setUserData(pLight);
	}

	private void setUnvisible(Sprite pSprite) {
		pSprite.setPosition(OUT_OF_SCREEN, OUT_OF_SCREEN);
		pSprite.setVisible(false);
		pSprite.setUserData(null);
	}

	@Override
	public void onCollisionDetected(ColliderType pType, Object pColeedee) {
		if(pType == ColliderType.Light && pColeedee instanceof Sprite) {
			Sprite sprite = (Sprite) pColeedee;
			mOnNextUpdateOperations.add(new DeferredRemovalOfSprite(sprite));
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(Sprite s : mSprites) {
			if(s.isVisible()) {
				sb.append('V');
			} else {
				sb.append('U');
			}
			sb.append('(').append(s.getX()).append(',').append(s.getY()).append(';').append(s.getUserData()).append(')').append('|');
		}
		sb.append(mLightManager);
		return sb.toString();
	}

	private class DeferredRemovalOfSprite implements Runnable {

		private final Sprite mSprite;

		public DeferredRemovalOfSprite(Sprite pSprite) {
			mSprite = pSprite;
		}

		@Override
		public void run() {
			mLightManager.removeLight((Light) mSprite.getUserData());
			setUnvisible(mSprite);
		}
	}
}
