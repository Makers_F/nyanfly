package com.makersf.games.nyanfly.adapters;

import org.andengine.engine.handler.IUpdateHandler;

import com.makersf.games.nyanfly.cavern.CavernProvider;

public class CavernProviderAdapter implements IUpdateHandler {

	private final CavernProvider mCavernProvider;

	public CavernProviderAdapter(CavernProvider pCavernProvider) {
		mCavernProvider = pCavernProvider;
	}

	@Override
	public void onUpdate(float pSecondsElapsed) {
		mCavernProvider.onUpdate(pSecondsElapsed);
	}

	@Override
	public void reset() {
		mCavernProvider.reset();
	}

}
