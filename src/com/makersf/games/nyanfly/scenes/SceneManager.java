package com.makersf.games.nyanfly.scenes;

import java.util.EnumMap;
import java.util.Map;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.scene.Scene;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.makersf.games.nyanfly.SetSceneCallback;
import com.makersf.games.nyanfly.assets.GRAPHIC_QUALITY;
import com.makersf.games.nyanfly.assets.GameResourcesManager;

public class SceneManager implements ISceneChangesHandler {

	private GameResourcesManager mResourcesManager;
	private GRAPHIC_QUALITY mQuality;
	private VertexBufferObjectManager mVBOManager;
	private Camera mCamera;
	private SetSceneCallback mSetSCeneCallback;

	private Map<SceneType, Scene> mScenes = new EnumMap<SceneType, Scene>(SceneType.class);
	private SceneType mCurrentScene;

	public SceneManager(Camera pCamera, GRAPHIC_QUALITY pQuality, GameResourcesManager pResourcesManager, VertexBufferObjectManager pVBOManager, SetSceneCallback pSetSceneCallback) {
		mCamera = pCamera;
		mQuality = pQuality;
		mResourcesManager = pResourcesManager;
		mVBOManager = pVBOManager;
		mSetSCeneCallback = pSetSceneCallback;

		mCurrentScene = SceneType.INTRO;
	}

	private Scene instantiateScene(SceneType pSceneType) {
		switch (pSceneType) {
			case CAVERN:
				return new CavernScene(mResourcesManager, mQuality, this, mVBOManager);
			case INTRO:
				return new IntroScene(mCamera, mResourcesManager, mQuality, this, mVBOManager);
		}
		throw new IllegalArgumentException("The SceneManager does not know how to instantiate a scene of type " + pSceneType);
	}

	public void setCurrent(SceneType pSceneType) {
		mCurrentScene = pSceneType;
	}

	public Scene getCurrent() {
		return get(mCurrentScene);
	}

	public Scene get(SceneType pSceneType) {
		if(mScenes.get(pSceneType) == null) {
			mScenes.put(pSceneType, instantiateScene(pSceneType));
		}
		return mScenes.get(pSceneType);
	}

	@Override
	public void requestSceneChange(SceneType pCurrentScene, SceneType pTargetScene, boolean pDestroyCurrent) {
		if (mCurrentScene == pCurrentScene) {
			mSetSCeneCallback.setScene(get(pTargetScene));
			mCurrentScene = pTargetScene;
			if (pDestroyCurrent) {
				mScenes.remove(pCurrentScene);
			}
		}
	}
}
