package com.makersf.games.nyanfly.scenes;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.scene.menu.item.SpriteMenuItem;
import org.andengine.entity.scene.menu.item.decorator.ScaleMenuItemDecorator;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.util.Log;

import com.makersf.games.nyanfly.assets.GRAPHIC_QUALITY;
import com.makersf.games.nyanfly.assets.GameResourcesManager;
import com.makersf.games.nyanfly.assets.GameResourcesManager.AssetsID;

public class IntroScene extends MenuScene {

	private final int MENU_PLAY = 0;
	private final int MENU_OPTIONS = 1;

	private final GameResourcesManager mResourcesManager;
	private final GRAPHIC_QUALITY mQuality;
	private final ISceneChangesHandler mSceneHandler;

	public IntroScene(Camera pCamera, GameResourcesManager pResourcesManager, GRAPHIC_QUALITY pQuality, ISceneChangesHandler pSceneHandler, VertexBufferObjectManager pVBOManager) {
		super(pCamera);
		mResourcesManager = pResourcesManager;
		mQuality = pQuality;
		mSceneHandler = pSceneHandler;

		this.setBackground(new Background(0.0f, 0.4f, 0.4f));

		IMenuItem playMenuItem = new ScaleMenuItemDecorator(new SpriteMenuItem(MENU_PLAY,
								mResourcesManager.getTextureRegion(AssetsID.PlayButtonTextureRegion), pVBOManager), 0.7f, 0.6f);
		IMenuItem optionsMenuItem = new ScaleMenuItemDecorator(new SpriteMenuItem(MENU_OPTIONS, 
				mResourcesManager.getTextureRegion(AssetsID.OptionsButtonTextureRegion), pVBOManager), 0.7f, 0.6f);

		this.addMenuItem(playMenuItem);
		this.addMenuItem(optionsMenuItem);
		this.buildAnimations();
Log.i("MENUSCENE", mQuality.toString());
		playMenuItem.setPosition(mQuality.WIDTH/2, 2/3.f * mQuality.HEIGHT);
		optionsMenuItem.setPosition(mQuality.WIDTH/2, 1/3.f * mQuality.HEIGHT);

		this.setOnMenuItemClickListener(new MenuListener());
	}

	private class MenuListener implements IOnMenuItemClickListener {
		@Override
		public boolean onMenuItemClicked(MenuScene pMenuScene, IMenuItem pMenuItem, float pMenuItemLocalX, float pMenuItemLocalY) {
			switch (pMenuItem.getID()) {
			case MENU_PLAY:
				mSceneHandler.requestSceneChange(SceneType.INTRO, SceneType.CAVERN, false);
				return true;
			case MENU_OPTIONS:
				return true;
			default:
				return false;
			}
		}
	}
}
