package com.makersf.games.nyanfly.scenes;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.primitive.DrawMode;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.makersf.andengine.extension.collisions.entity.sprite.PixelPerfectSprite;
import com.makersf.andengine.extension.collisions.opengl.texture.region.PixelPerfectTextureRegion;
import com.makersf.games.nyanfly.adapters.CavernProviderAdapter;
import com.makersf.games.nyanfly.adapters.LightManagerAdapter;
import com.makersf.games.nyanfly.assets.GRAPHIC_QUALITY;
import com.makersf.games.nyanfly.assets.GameResourcesManager;
import com.makersf.games.nyanfly.assets.GameResourcesManager.AssetsID;
import com.makersf.games.nyanfly.cavern.CavernBoundsGenerator;
import com.makersf.games.nyanfly.cavern.CavernProvider;
import com.makersf.games.nyanfly.perks.LightManager;
import com.makersf.games.nyanfly.physics.CollisionCheckHandler;
import com.makersf.games.nyanfly.physics.ICollisionCallback;
import com.makersf.games.nyanfly.player.LightComponent;
import com.makersf.games.nyanfly.player.PlayerAttributes;
import com.makersf.games.nyanfly.player.PlayerComponent;
import com.makersf.games.nyanfly.player.ScreenTouchSensor;
import com.makersf.games.nyanfly.player.characters.nyancat.NyanCatOnAreaTouchListener;
import com.makersf.games.nyanfly.player.characters.nyancat.NyanCatPlayerAttributes;
import com.makersf.games.nyanfly.player.characters.nyancat.NyanCatUpdateHandler;
import com.makersf.games.nyanfly.player.movement.BoundEnsurer;
import com.makersf.games.nyanfly.player.movement.IOnMovementListener;
import com.makersf.games.nyanfly.player.movement.Movement;
import com.makersf.games.nyanfly.player.movement.SpritePositionSynchronizer;
import com.makersf.games.nyanfly.shapes.mesh.CavernBoundMesh;
import com.makersf.games.nyanfly.shapes.mesh.CavernMeshBufferData;
import com.makersf.games.nyanfly.shapes.mesh.ShadowMesh;
import com.makersf.games.nyanfly.shapes.shader.CavernBinder;
import com.makersf.games.nyanfly.shapes.shader.ShadowBinder;

public class CavernScene extends Scene implements ICollisionCallback {

	private static final int MAX_NUMBER_OF_LIGHTS = 5;

	private final GameResourcesManager mResourcesManager;
	private final GRAPHIC_QUALITY mQuality;
	private final ISceneChangesHandler mSceneHandler;

	private CavernProvider mCavernGenerator;
	private PlayerComponent mPlayer;
	private LightManager mLightManager;

	public CavernScene(GameResourcesManager pResourcesManager, GRAPHIC_QUALITY pQuality, ISceneChangesHandler pSceneHandler, VertexBufferObjectManager pVBOManager) {
		super();
		mResourcesManager = pResourcesManager;
		mQuality = pQuality;
		mSceneHandler = pSceneHandler;

		this.setBackground(new Background(0.2f, 0.8f, 1f));//(0.1f, 0.1f, 0.1f));

		//set up the player component
		PixelPerfectSprite playerSprite = new PixelPerfectSprite(0, 0, mResourcesManager.getTyped(AssetsID.BasePlayerTextureRegion, PixelPerfectTextureRegion.class), pVBOManager);

		PlayerAttributes playerAttributes = new NyanCatPlayerAttributes();
		Movement movement = new Movement(playerSprite.getWidth() / mQuality.WIDTH, playerSprite.getHeight() / mQuality.HEIGHT, playerAttributes);
		movement.addMovementListener(new SpritePositionSynchronizer(playerSprite, mQuality), IOnMovementListener.POSITION_CHANGE);
		movement.addMovementListener(new BoundEnsurer(), IOnMovementListener.POSITION_CHANGE);

		ITouchArea touchArea = new ScreenTouchSensor(0, 0, mQuality.WIDTH, 0.9f * mQuality.HEIGHT,
				new NyanCatOnAreaTouchListener(movement));
		this.registerTouchArea(touchArea);

		LightComponent lightComponent = new LightComponent(playerAttributes);
		mPlayer = new PlayerComponent(movement, playerSprite, lightComponent, mQuality);
		mCavernGenerator = new CavernBoundsGenerator(mQuality.VERTICES_PER_SCREEN, movement);
		mLightManager = new LightManager(MAX_NUMBER_OF_LIGHTS, mCavernGenerator, movement);
		LightManagerAdapter lightManagerAdapter = new LightManagerAdapter(mLightManager,
														mResourcesManager.getTyped(AssetsID.LightBulbTextureRegion, PixelPerfectTextureRegion.class),
														mQuality, pVBOManager);

		IUpdateHandler playerUpdateHandler = new NyanCatUpdateHandler(movement);
		this.registerUpdateHandler(playerUpdateHandler);
		this.registerUpdateHandler(new CavernProviderAdapter(mCavernGenerator));
		this.registerUpdateHandler(lightManagerAdapter);
		this.registerUpdateHandler(lightComponent);

		//TODO set the an autopilot for the first 2-3 seconds to make the player always stay in the middle of the cavern
		final float maxY = mCavernGenerator.sample(movement.X(), true, mQuality.WIDTH);
		final float minY = mCavernGenerator.sample(movement.X(), false, mQuality.WIDTH);
		movement.setY( (maxY + minY)/2);
		movement.notifyChange(IOnMovementListener.POSITION_CHANGE);

		float[] cavernBufferData = CavernMeshBufferData.getBufferData(mQuality.WIDTH, mQuality.HEIGHT, mQuality.VERTICES_PER_SCREEN);
		CavernBoundMesh LowerCavern = new CavernBoundMesh(0, 0, mResourcesManager.getTextureRegion(AssetsID.LowerCavernTextureRegion), false, cavernBufferData, cavernBufferData.length/2,
				DrawMode.TRIANGLE_STRIP, mResourcesManager.getShader(AssetsID.CavernShader), pVBOManager);

		CavernBoundMesh UpperCavern = new CavernBoundMesh(0, mQuality.HEIGHT, mResourcesManager.getTextureRegion(AssetsID.UpperCavernTextureRegion), false, cavernBufferData, cavernBufferData.length/2,
				DrawMode.TRIANGLE_STRIP, mResourcesManager.getShader(AssetsID.CavernShader), pVBOManager);

		ShadowMesh shadowMesh = new ShadowMesh(mQuality.WIDTH/2, mQuality.HEIGHT/2, mQuality.WIDTH, mQuality.HEIGHT, mResourcesManager.getShader(AssetsID.ShadowShader), pVBOManager);

		LowerCavern.setBindCallback(new CavernBinder(mCavernGenerator, mQuality, false));

		UpperCavern.setBindCallback(new CavernBinder(mCavernGenerator, mQuality, true));

		shadowMesh.setBindCallback(new ShadowBinder(mPlayer, mLightManager, mQuality));

		CollisionCheckHandler collisionChecker = new CollisionCheckHandler(playerSprite, mQuality);

		this.attachChild(LowerCavern);
		this.attachChild(UpperCavern);
		this.attachChild(mPlayer.getSprite());
		for(Sprite sprite : lightManagerAdapter.getLightsSprites()) {
			this.attachChild(sprite);
			collisionChecker.addCollider(ColliderType.Light, sprite);
		}
		this.attachChild(shadowMesh);
		this.registerUpdateHandler(collisionChecker);
		collisionChecker.setCavern(mCavernGenerator);
		//collisionChecker.registerCollisionCallback(ColliderType.Cavern, this);
		collisionChecker.registerCollisionCallback(ColliderType.Light, lightManagerAdapter);
		collisionChecker.registerCollisionCallback(ColliderType.Light, lightComponent);
	}

	@Override
	public void onCollisionDetected(ColliderType pType, Object pColeedee) {
		mSceneHandler.requestSceneChange(SceneType.CAVERN, SceneType.INTRO, true);
	}
}
