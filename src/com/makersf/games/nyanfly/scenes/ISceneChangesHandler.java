package com.makersf.games.nyanfly.scenes;

public interface ISceneChangesHandler {

	public void requestSceneChange(SceneType pCurrentScene, SceneType pTargetScene, boolean pDestroyCurrent);
}
