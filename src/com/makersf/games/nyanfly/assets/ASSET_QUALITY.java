package com.makersf.games.nyanfly.assets;

public enum ASSET_QUALITY {
	HIGH_QUALITY("high", 1.5f),
	MEDIUM_QUALITY("medium", 1.f),
	LOW_QUALITY("low", 1/1.5f);

	private final String mBaseString;
	public final float TEXTURE_DIMENSION_MULTIPLIER;

	ASSET_QUALITY(String pBaseString, final float pTextureDimensionMultiplier) {
		mBaseString = pBaseString;
		TEXTURE_DIMENSION_MULTIPLIER = pTextureDimensionMultiplier;
	}

	/**
	 * Replace pWordToReplace in pAssetPath with the correct name of the directory holding the
	 * assets of the desired quality.
	 *
	 * It is used to resolve complex path that needs nested directories.
	 * To simply prepone the direcotry quality name use assetResolvedPath(String).
	 * @param pAssetPath The path to modify.
	 * @param pWordToReplace The world to replace with the directory quality name.
	 * @return The resolved path.
	 */
	public String assetResolvedPath(final String pAssetPath, final String pWordToReplace) {
		return pAssetPath.replace(pWordToReplace, mBaseString);
	}

	/**
	 * Prepones the directory quality name to the pAssetPath.
	 * @param pAssetPath The path to postpone to the directory quality name.
	 * @return The resolved path.
	 */
	public String assetResolvedPath(final String pAssetPath) {
		if(pAssetPath.startsWith(java.io.File.separator, 0))
			return mBaseString + pAssetPath;
		else
			return mBaseString + java.io.File.separator + pAssetPath;

	}
}