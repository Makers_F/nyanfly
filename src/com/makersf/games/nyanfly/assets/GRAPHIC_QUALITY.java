package com.makersf.games.nyanfly.assets;



public enum GRAPHIC_QUALITY {

	HIGH(0x0003, 1200, 720, ASSET_QUALITY.HIGH_QUALITY, 120),
	MEDIUM(0x0002, 800, 480, ASSET_QUALITY.MEDIUM_QUALITY, 50),
	LOW(0x0001, 533, 320, ASSET_QUALITY.LOW_QUALITY, 25);

	public final int WIDTH;
	public final int HEIGHT;
	public final ASSET_QUALITY ASSETS_QUALITY;
	public final int QUALITY_CODE;
	public final int VERTICES_PER_SCREEN;

	GRAPHIC_QUALITY(final int pQualityCode, final int pScreenWidth, final int pScreenHeight, final ASSET_QUALITY pAssetQuality, final int pVerticesPerScreen) {
		QUALITY_CODE = pQualityCode;
		WIDTH = pScreenWidth;
		HEIGHT = pScreenHeight;
		ASSETS_QUALITY = pAssetQuality;
		VERTICES_PER_SCREEN = pVerticesPerScreen;
	}

	public int normalizeTexDim(final int pTextureDimensionToNormalize) {
		return Math.round(pTextureDimensionToNormalize * ASSETS_QUALITY.TEXTURE_DIMENSION_MULTIPLIER);
	}

	public static GRAPHIC_QUALITY getRenderingQualityFromScreenDimension(final int pScreenWidth, final int pScreenHeight) {
		final float ratio = 5/3.f;

		/* Margin for possible borders, decorations, bars that prevent the app from running in full screen.
		 * Since i don't want to run with a 533x320 on a 800x480 because (e.g.) 20px were cut off by a small bar on top of the screen
		 * i keep a little margin.
		 */
		int heighMargin = 60;
		if(pScreenWidth >= HIGH.WIDTH - heighMargin*ratio || pScreenHeight >= HIGH.HEIGHT - heighMargin)
			return HIGH;

		heighMargin = 40;
		if(pScreenWidth >= MEDIUM.WIDTH - heighMargin*ratio  || pScreenHeight >= MEDIUM.HEIGHT - heighMargin)
			return MEDIUM;

		return LOW;
	}

	@Override
	public String toString() {
		return "Quality: " + this.name() + " [" + WIDTH + "x" + HEIGHT + "]. Assets: " + ASSETS_QUALITY.name();
	}
}
