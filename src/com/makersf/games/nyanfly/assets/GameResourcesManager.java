package com.makersf.games.nyanfly.assets;

import java.util.EnumMap;
import java.util.Map;

import org.andengine.opengl.shader.ShaderProgram;
import org.andengine.opengl.texture.region.ITextureRegion;

public class GameResourcesManager {

	public static final int CAVERN_TEXTURE_WIDTH = 512;
	public static final int CAVERN_TEXTURE_HEIGHT = 256;
	public static final int MENU_BUTTONS_TEXTURE_WIDTH = 512;
	public static final int MENU_BUTTONS_TEXTURE_HEIGHT = 128;
	public static final int PLAYER_TEXTURE_WIDTH = 53;
	public static final int PLAYER_TEXTURE_HEIGHT = 30;
	public static final int LIGHTBULB_TEXTURE_WIDTH = 48;
	public static final int LIGHTBULB_TEXTURE_HEIGHT = 48;

	private Map<AssetsID, Object> mResources = new EnumMap<GameResourcesManager.AssetsID, Object>(AssetsID.class);

	public void putTextureRegion(AssetsID pAssetsID, ITextureRegion pTextureRegion) {
		putOnce(pAssetsID, pTextureRegion);
	}

	public ITextureRegion getTextureRegion(AssetsID pTextureRegionID) {
		return getTyped(pTextureRegionID, ITextureRegion.class);
	}

	public void putShader(AssetsID pShaderID, ShaderProgram pShaderProgram) {
		putOnce(pShaderID, pShaderProgram);
	}

	public ShaderProgram getShader(AssetsID pTextureRegionID) {
		return getTyped(pTextureRegionID, ShaderProgram.class);
	}

	public <T> T getTyped(AssetsID pID, Class<T> clazz) {
		Object typedObject = mResources.get(pID);
		if(clazz.isAssignableFrom(typedObject.getClass())) {
			return clazz.cast(typedObject);
		} else {
			throw new IllegalArgumentException("The ID: " + pID + " is not mapped to a " + clazz.getCanonicalName() + ". It's mapped instead to " + typedObject.getClass().getCanonicalName());
		}
	}

	// ===========================================================
	// Private Methods
	// ===========================================================

	private void putOnce(AssetsID pID, Object pTypedObject) {
		if(mResources.get(pID) == null) {
			mResources.put(pID, pTypedObject);
		} else {
			throw new IllegalArgumentException("It is already present an element mapped to " + pID + ". You tried to remap it to " + pTypedObject);
		}
	}

	// ===========================================================
	// Public class
	// ===========================================================

	public static enum AssetsID {
		LowerCavernTextureRegion,
		UpperCavernTextureRegion,
		OptionsButtonTextureRegion,
		PlayButtonTextureRegion,
		BasePlayerTextureRegion,
		LightBulbTextureRegion,
		CavernShader,
		ShadowShader;
	}
}
