package com.makersf.games.nyanfly.player;

import org.andengine.entity.sprite.Sprite;

import com.makersf.games.nyanfly.assets.GRAPHIC_QUALITY;
import com.makersf.games.nyanfly.player.movement.Movement;

public class PlayerComponent {

	private final Sprite mSprite;
	private final Movement mMovement;
	private final GRAPHIC_QUALITY mQuality;
	private final LightComponent mLightComponent;

	public PlayerComponent(Movement pMovement, Sprite pSprite, LightComponent pLightComponent, GRAPHIC_QUALITY pQuality) {
		mMovement = pMovement;
		mSprite = pSprite;
		mQuality = pQuality;
		mLightComponent = pLightComponent;
	}

	public Sprite getSprite() {
		return mSprite;
	}

	public LightComponent getLightComponent() {
		return mLightComponent;
	}

	public void setX(float pX) {
		mMovement.setX(pX / mQuality.WIDTH);
	}

	public void setY(float pY) {
		mMovement.setY(pY / mQuality.HEIGHT);
	}

	public void setPosition(float pX, float pY) {
		mMovement.setX(pX);
		mMovement.setY(pY);
	}
}
