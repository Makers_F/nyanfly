package com.makersf.games.nyanfly.player;

import it.francesco.utils.math.MathUtilities;

import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.sprite.Sprite;

import com.makersf.games.nyanfly.perks.Light;
import com.makersf.games.nyanfly.physics.ICollisionCallback;

public class LightComponent implements IUpdateHandler, ICollisionCallback {

	private static final float LIGHT_ABSORBION_EFFICENCY = 0.5f;

	// mLightIntensity is SCREEN INDEPENDENT. The shadowbinder automatically scale it with the width of the screen
	private float mLightIntensity;
	private float mFalloffPerSecond;
	private final PlayerAttributes mAttributes;

	public LightComponent(PlayerAttributes pAttributes) {
		mAttributes = pAttributes;
		reset();
	}

	@Override
	public void onUpdate(float pSecondsElapsed) {
		mLightIntensity -= mFalloffPerSecond * pSecondsElapsed;
		mLightIntensity = MathUtilities.clamp(mLightIntensity,
									mAttributes.BASE_LIGHT_INTENSITY * mAttributes.MINIMUM_LIGH_PERCENT,
									mAttributes.BASE_LIGHT_INTENSITY * mAttributes.MAXIMUM_LIGHT_PERCENT);
	}

	@Override
	public void reset() {
		mLightIntensity = mAttributes.BASE_LIGHT_INTENSITY;
		mFalloffPerSecond = mLightIntensity / mAttributes.BASE_LIGHT_FALLOFF_TIME;
	}

	public float getLightIntensity() {
		return mLightIntensity;
	}

	@Override
	public void onCollisionDetected(ColliderType pType, Object pColeedee) {
		if(pType == ColliderType.Light && pColeedee instanceof Sprite) {
			Sprite sprite = (Sprite) pColeedee;
			Light light = (Light) sprite.getUserData();
			mLightIntensity += light.intensity * LIGHT_ABSORBION_EFFICENCY;
			MathUtilities.clamp(mLightIntensity,
					mAttributes.BASE_LIGHT_INTENSITY * mAttributes.MINIMUM_LIGH_PERCENT,
					mAttributes.BASE_LIGHT_INTENSITY * mAttributes.MAXIMUM_LIGHT_PERCENT);
		}
		
	}
}
