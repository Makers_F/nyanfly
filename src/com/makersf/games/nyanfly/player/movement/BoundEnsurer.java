package com.makersf.games.nyanfly.player.movement;

import it.francesco.utils.math.MathUtilities;

public class BoundEnsurer implements IOnMovementListener {

	@Override
	public void onChangedMovement(Movement pMovement, int pListenerCode) {
		if((pListenerCode & POSITION_CHANGE) == POSITION_CHANGE) {
			pMovement.setX(MathUtilities.clamp(pMovement.X(), 0, 1.f));
			pMovement.setY(MathUtilities.clamp(pMovement.Y(), 0, 1.f));
		}
	}
}
