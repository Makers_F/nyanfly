package com.makersf.games.nyanfly.player.movement;

import org.andengine.entity.sprite.Sprite;

import com.makersf.games.nyanfly.assets.GRAPHIC_QUALITY;


public class SpritePositionSynchronizer implements IOnMovementListener {

	private final Sprite mSprite;
	private final GRAPHIC_QUALITY mQuality;

	public SpritePositionSynchronizer(Sprite pSprite, GRAPHIC_QUALITY pQuality) {
		mSprite = pSprite;
		mQuality = pQuality;
		
	}
	@Override
	public void onChangedMovement(Movement pMovement, int pListenerCode) {
		if((pListenerCode & POSITION_CHANGE) == POSITION_CHANGE) {
			mSprite.setX(pMovement.X() * mQuality.WIDTH);
			mSprite.setY(pMovement.Y() * mQuality.HEIGHT);
		}
	}

}
