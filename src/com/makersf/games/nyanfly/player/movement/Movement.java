package com.makersf.games.nyanfly.player.movement;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.makersf.games.nyanfly.cavern.IPlayerInformationsProvider;
import com.makersf.games.nyanfly.player.IMovingControl;
import com.makersf.games.nyanfly.player.PlayerAttributes;

import android.util.Pair;

public class Movement implements IMovingControl, IPlayerInformationsProvider {

	private DIRECTION direction;

	private float width;
	private float height;
	private float X;
	private float Y;
	private float velocityX;
	private float velocityY;
	private PlayerAttributes playerAttributes;

	private List<Pair<Integer,IOnMovementListener>> mMovementListeners = new ArrayList<Pair<Integer,IOnMovementListener>>();

	public Movement(float pPlayerRelativeWidth, float pPlayerRelativeHeight, PlayerAttributes pPlayerAttributes) {
		width = pPlayerRelativeWidth;
		height = pPlayerRelativeHeight;
		playerAttributes = pPlayerAttributes;
		X = pPlayerAttributes.PLAYER_X_POSITION;
		Y = 0;
		velocityX = 1.f / pPlayerAttributes.BASE_VELOCITY_X_TIME;
		velocityY = 0;
		direction = DIRECTION.DOWN;
	}

	public void addMovementListener(IOnMovementListener pMovementListener, int pListenerCode) {
		mMovementListeners.add(new Pair<Integer, IOnMovementListener>(pListenerCode, pMovementListener));
	}

	public boolean removeMovementListener(IOnMovementListener pMovementListener, int pListenerCode) {
		Iterator<Pair<Integer,IOnMovementListener>> iterator = mMovementListeners.listIterator();
		while(iterator.hasNext()) {
			if(iterator.next().second == pMovementListener) {
				iterator.remove();
				return true;
			}
		}
		return false;
	}

	public void clearMovementListeners() {
		mMovementListeners.clear();
	}

	public float X() {
		return X;
	}

	public float Y() {
		return Y;
	}

	public void setX(float x) {
		if(x != X) {
			X = x;
			onMovementChange(IOnMovementListener.POSITION_CHANGE);
		}
	}

	public void setY(float y) {
		if( y != Y) {
			Y = y;
			onMovementChange(IOnMovementListener.POSITION_CHANGE);
		}
	}

	public void setPosition(float pX, float pY) {
		if(pX != X || pY != Y) {
			X = pX;
			Y = pY;
			onMovementChange(IOnMovementListener.POSITION_CHANGE);
		}
	}

	public void setVelocityX(float pVelocityX) {
		if( pVelocityX != velocityX) {
			velocityX = pVelocityX;
			onMovementChange(IOnMovementListener.VELOCITY_CHANGE);
		}
	}

	public float getVelocityY() {
		return velocityY;
	}

	public void setVelocityY(float pVelocityY) {
		if( pVelocityY != velocityY) {
			velocityY = pVelocityY;
			onMovementChange(IOnMovementListener.VELOCITY_CHANGE);
		}
	}

	public float getAccelerationX() {
		return 1.f / playerAttributes.BASE_ACCELERATION_X_TIME;
	}

	public float getAccelerationY() {
		return (1.f / playerAttributes.BASE_ACCELERATION_Y_TIME);
	}

	@Override
	public void setDirection(DIRECTION pDirection) {
		if( !direction.equals(pDirection)) {
			direction = pDirection;
			onMovementChange(IOnMovementListener.DIRECTION_CHANGE);
		}
	}

	@Override
	public DIRECTION getDirection() {
		return direction;
	}

	@Override
	public float getWidth() {
		return width;
	}

	@Override
	public float getHeight() {
		return height;
	}

	public void notifyChange(int pChangeCode) {
		onMovementChange(pChangeCode);
	}

	private void onMovementChange(final int pChangeCode) {
		for(Pair<Integer,IOnMovementListener> pair : mMovementListeners) {
			int code = pair.first;
			if( (code & pChangeCode ) != 0)
				pair.second.onChangedMovement(this, pChangeCode);
		}
	}

	@Override
	public float getPercentOfScreenTraveledPerSecond() {
		return velocityX;
	}

	@Override
	public float getMaxHeightDeltaIn(float pSeconds) {
		/*
		 * Worst possible case: the player is going down at max velocity and then starts to go up. This is the integral of the velocity in dt.
		 * If the velocity is opposite to the acceleration, we have a adjusting coefficient of 2 to make it more responsive in changing direction
		 * If pSeconds is less than timeLimit, then we use the adjusting coefficient for all the interval. If it's bigger, we use it only for the first timeLimit
		 */
		float maxAcc = 1.f / playerAttributes.BASE_ACCELERATION_Y_TIME;
		float maxV = getVelocityYMaxDown();
		float timeLimit = maxV / (2 * maxAcc);
		if ( pSeconds <= timeLimit) {
			return Math.max(-maxV * pSeconds + maxAcc * pSeconds * pSeconds, 0);
		} else {
			return Math.max(-maxV * timeLimit + maxAcc * timeLimit * timeLimit +
					0.5f * maxAcc * (pSeconds - timeLimit) * (pSeconds - timeLimit), 0);
		}
	}

	@Override
	public float getMinHeightDeltaIn(float pSeconds) {
		/* See above*/
		float maxAcc = 1.f / playerAttributes.BASE_ACCELERATION_Y_TIME;
		float maxV = getVelocityYMaxDown();
		float timeLimit = maxV / (2 * maxAcc);
		if ( pSeconds <= timeLimit) {
			return Math.max(-maxV * pSeconds + maxAcc * pSeconds * pSeconds, 0);
		} else {
			return Math.max(-maxV * timeLimit + maxAcc * timeLimit * timeLimit +
					0.5f * maxAcc * (pSeconds - timeLimit) * (pSeconds - timeLimit), 0);
		}
	}

	public float getVelocityYMaxDown() {
		return - 1.f / playerAttributes.BASE_MIN_DOWN_Y_TIME;
	}

	public float getVelocityYMaxUp() {
		return 1.f /  playerAttributes.BASE_MIN_UP_Y_TIME;
	}
}
