package com.makersf.games.nyanfly.player.movement;


public interface IOnMovementListener {
	public static final int POSITION_CHANGE = 1;
	public static final int VELOCITY_CHANGE = 1 << 1;
	public static final int ACCELERATION_CHANGE = 1 << 2;
	public static final int DIRECTION_CHANGE = 1 << 3;
	
	public void onChangedMovement(Movement pMovement, int pListenerCode);
}
