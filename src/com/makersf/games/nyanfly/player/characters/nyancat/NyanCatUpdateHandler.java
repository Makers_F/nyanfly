package com.makersf.games.nyanfly.player.characters.nyancat;

import it.francesco.utils.math.MathUtilities;

import org.andengine.engine.handler.IUpdateHandler;

import com.makersf.games.nyanfly.player.IMovingControl.DIRECTION;
import com.makersf.games.nyanfly.player.movement.Movement;

public class NyanCatUpdateHandler implements IUpdateHandler {

	private Movement mMovement;

	public NyanCatUpdateHandler(Movement pMovement) {
		mMovement = pMovement;
	}

	@Override
	public void onUpdate(float pSecondsElapsed) {
		DIRECTION direction = mMovement.getDirection();
		/* If the velocity points to the opposite direction of isGoingUp, than double the acceleration.
		 * I do this so that it is faster to change direction.
		 */
		float adjustament = (mMovement.getDirection().multiplier * mMovement.getVelocityY() < 0) ? 2 : 1;
		float newVelocityY = mMovement.getVelocityY() +  direction.multiplier * adjustament * mMovement.getAccelerationY() * pSecondsElapsed;
		mMovement.setVelocityY(MathUtilities.clamp(newVelocityY, mMovement.getVelocityYMaxDown(), mMovement.getVelocityYMaxUp()));
		float newY = mMovement.Y() + mMovement.getVelocityY() * pSecondsElapsed;
		mMovement.setY(newY);
	}

	@Override
	public void reset() {
		
	}

}
