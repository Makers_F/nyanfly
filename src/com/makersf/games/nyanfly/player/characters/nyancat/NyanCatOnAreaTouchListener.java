package com.makersf.games.nyanfly.player.characters.nyancat;

import org.andengine.entity.scene.IOnAreaTouchListener;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.input.touch.TouchEvent;

import com.makersf.games.nyanfly.player.IMovingControl;
import com.makersf.games.nyanfly.player.IMovingControl.DIRECTION;

public class NyanCatOnAreaTouchListener implements IOnAreaTouchListener {

	private IMovingControl mMovingControl;

	public NyanCatOnAreaTouchListener(IMovingControl pMovingControl) {
		mMovingControl = pMovingControl;
	}

	@Override
	public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
			ITouchArea pTouchArea, float pTouchAreaLocalX,
			float pTouchAreaLocalY) {
		if(pSceneTouchEvent.isActionUp() || pSceneTouchEvent.isActionCancel() || pSceneTouchEvent.isActionOutside()) {
			mMovingControl.setDirection(DIRECTION.DOWN);
			return false;
		} else {
			mMovingControl.setDirection(DIRECTION.UP);
			return true;
		}
	}

}
