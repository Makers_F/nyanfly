package com.makersf.games.nyanfly.player.characters.nyancat;

import com.makersf.games.nyanfly.player.PlayerAttributes;

public class NyanCatPlayerAttributes extends PlayerAttributes {

	public NyanCatPlayerAttributes(){
		super.PLAYER_X_POSITION = 0.1f;
		super.BASE_VELOCITY_X_TIME = 5;
		super.BASE_MIN_UP_Y_TIME = 3;
		super.BASE_MIN_DOWN_Y_TIME = 3;
		super.BASE_ACCELERATION_X_TIME = 60;
		super.BASE_ACCELERATION_Y_TIME = 0.35f;
		super.BASE_LIGHT_INTENSITY = 0.15f;
		super.BASE_LIGHT_FALLOFF_TIME = 4;
		super.MINIMUM_LIGH_PERCENT = 0.6f;
		super.MAXIMUM_LIGHT_PERCENT = 2.f;
	}
}
