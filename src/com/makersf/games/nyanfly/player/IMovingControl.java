package com.makersf.games.nyanfly.player;

public interface IMovingControl {
	public void setDirection(DIRECTION pDirection);
	public DIRECTION getDirection();

	public static enum DIRECTION {
		UP(1),
		DOWN(-1);

		public final int multiplier;

		DIRECTION(int value) {
			multiplier = value;
		}

		public DIRECTION invert() {
			if(this.equals(DOWN))
				return UP;
			else
				return DOWN;
		}
	}
}
