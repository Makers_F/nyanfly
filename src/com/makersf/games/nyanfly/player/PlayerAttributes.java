package com.makersf.games.nyanfly.player;

import com.makersf.games.nyanfly.assets.GRAPHIC_QUALITY;

public abstract class PlayerAttributes {
	/** % of screen width [pure number] **/
	public float PLAYER_X_POSITION;
	/** how much time takes to travel the width of the screen [s] **/
	public float BASE_VELOCITY_X_TIME;
	/** how much time takes to travel the height of the screen at the maximum velocity. Bottom -> Up [s] **/
	public float BASE_MIN_UP_Y_TIME;
	/** how much time takes to travel the height of the screen at the maximum velocity. Up -> Bottom [s] **/
	public float BASE_MIN_DOWN_Y_TIME;
	/** how much time takes to double the velocity generated by BASE_FREQUENCY_X [s] **/
	public float BASE_ACCELERATION_X_TIME;
	/** how much time takes to increase the velocity by BASE_MIN_UP_Y_FREQUENCY [s] **/
	public float BASE_ACCELERATION_Y_TIME;
	/** how intense is the light when the player spawns in % of screen width [pure number] **/
	public float BASE_LIGHT_INTENSITY;
	/** how long it takes for the light to fall off to 0 intensity [s] **/
	public float BASE_LIGHT_FALLOFF_TIME;
	/** the minimum % of BASE_LIGHT_INTENSITY. The player light should not get lower that that **/
	public float MINIMUM_LIGH_PERCENT;
	/** the maximum % of BASE_LIGHT_INTENSITY. The player light should not get higher that that **/
	public float MAXIMUM_LIGHT_PERCENT;

	public float convertToResolutionDependant(Attribute pAttribute, GRAPHIC_QUALITY pQuality) {
		switch (pAttribute) {
			case X_POSITION:
				return pQuality.WIDTH * PLAYER_X_POSITION;
			case X_VELOCITY:
				return pQuality.WIDTH / BASE_VELOCITY_X_TIME;
			case MAX_UP_VELOCITY:
				return pQuality.HEIGHT / BASE_MIN_UP_Y_TIME;
			case MAX_DOWN_VELOCITY:
				return - pQuality.HEIGHT / BASE_MIN_DOWN_Y_TIME;
			case X_ACCELERATION:
				return convertToResolutionDependant(Attribute.X_VELOCITY, pQuality) / BASE_ACCELERATION_X_TIME;
			case Y_ACCELERATION:
				return convertToResolutionDependant(Attribute.MAX_UP_VELOCITY, pQuality) / BASE_ACCELERATION_Y_TIME;
			case LIGHT_INTENSITY:
				return pQuality.WIDTH * BASE_LIGHT_INTENSITY;
			default:
				throw new NullPointerException("pAttribute is null. " + pAttribute);
				
		}
	}

	public static enum Attribute {
		X_POSITION(),
		X_VELOCITY(),
		MAX_UP_VELOCITY(),
		MAX_DOWN_VELOCITY(),
		X_ACCELERATION(),
		Y_ACCELERATION(),
		LIGHT_INTENSITY();
	}
}
