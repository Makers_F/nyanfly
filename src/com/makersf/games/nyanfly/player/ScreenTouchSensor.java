package com.makersf.games.nyanfly.player;

import org.andengine.entity.scene.IOnAreaTouchListener;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.input.touch.TouchEvent;

public class ScreenTouchSensor implements ITouchArea {

	private float x;
	private float y;
	private float width;
	private float height;
	private IOnAreaTouchListener listener;

	public ScreenTouchSensor(float pX, float pY, float pWidth, float pHeight, IOnAreaTouchListener pListener) {
		x = pX;
		y = pY;
		width = pWidth;
		height = pHeight;
		listener = pListener;
	}

	@Override
	public boolean contains(float pX, float pY) {
		float[] locC = convertSceneCoordinatesToLocalCoordinates(pX, pY); 
		return 0 < locC[0] && locC[0] < width && 0 < locC[1] && locC[1] < height;
	}

	@Override
	public float[] convertSceneCoordinatesToLocalCoordinates(float pX, float pY) {
		return new float[]{pX - x, pY - y};
	}

	@Override
	public float[] convertLocalCoordinatesToSceneCoordinates(float pX, float pY) {
		return new float[]{pX + x, pY + y};
	}

	@Override
	public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
			float pTouchAreaLocalX, float pTouchAreaLocalY) {
		return listener.onAreaTouched(pSceneTouchEvent, this, pTouchAreaLocalX, pTouchAreaLocalY);
	}

}
