package com.makersf.games.nyanfly;

import java.io.IOException;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.util.FPSLogger;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.ui.activity.BaseGameActivity;

import com.makersf.andengine.extension.collisions.opengl.texture.region.PixelPerfectTextureRegionFactory;
import com.makersf.games.nyanfly.assets.GRAPHIC_QUALITY;
import com.makersf.games.nyanfly.assets.GameResourcesManager;
import com.makersf.games.nyanfly.assets.GameResourcesManager.AssetsID;
import com.makersf.games.nyanfly.scenes.SceneManager;
import com.makersf.games.nyanfly.scenes.SceneType;
import com.makersf.games.nyanfly.shapes.shader.CavernShaderProgram;
import com.makersf.games.nyanfly.shapes.shader.FileShaderSource;
import com.makersf.games.nyanfly.shapes.shader.ShadowShaderProgram;
import com.makersf.games.nyanfly.shapes.shader.ShapesShaderProgramCostants;

import android.util.DisplayMetrics;

public class MainActivity extends BaseGameActivity {
	// ===========================================================
	// Constants
	// ===========================================================

	// ===========================================================
	// Fields
	// ===========================================================

	private GRAPHIC_QUALITY mQuality;
	private Camera mCamera;

	private GameResourcesManager mResourcesManager;
	private SceneManager mSceneManager;

	// ===========================================================
	// Constructors
	// ===========================================================

	@Override
	public EngineOptions onCreateEngineOptions() {

		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);

		mQuality = GRAPHIC_QUALITY.getRenderingQualityFromScreenDimension(Math.max(metrics.widthPixels, metrics.heightPixels), Math.min(metrics.widthPixels, metrics.heightPixels));

		mCamera = new Camera(0, 0, mQuality.WIDTH, mQuality.HEIGHT);

		return new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED, new RatioResolutionPolicy(mQuality.WIDTH, mQuality.HEIGHT), mCamera);
	}

	@Override
	public void onCreateResources(
			OnCreateResourcesCallback pOnCreateResourcesCallback)
			throws IOException {

		mResourcesManager = new GameResourcesManager();
		mSceneManager = new SceneManager(mCamera, mQuality, mResourcesManager, getVertexBufferObjectManager(), new SetSceneCallback(getEngine()));

		final BitmapTextureAtlas cavernAtlas = new BitmapTextureAtlas(getTextureManager(),
							mQuality.normalizeTexDim(GameResourcesManager.CAVERN_TEXTURE_WIDTH),
							mQuality.normalizeTexDim(GameResourcesManager.CAVERN_TEXTURE_HEIGHT) * 2, //two times the textures, two times the height must be!
							TextureOptions.BILINEAR_PREMULTIPLYALPHA);

		mResourcesManager.putTextureRegion(AssetsID.LowerCavernTextureRegion,
				BitmapTextureAtlasTextureRegionFactory.createFromAsset(cavernAtlas, getAssets(), mQuality.ASSETS_QUALITY.assetResolvedPath("gfx/$Q/cavern/terrain.png", "$Q"), 0, 0));

		mResourcesManager.putTextureRegion(AssetsID.UpperCavernTextureRegion,
				BitmapTextureAtlasTextureRegionFactory.createFromAsset(cavernAtlas, getAssets(), mQuality.ASSETS_QUALITY.assetResolvedPath("gfx/$Q/cavern/cavern.png", "$Q"), 0, mQuality.normalizeTexDim(GameResourcesManager.CAVERN_TEXTURE_HEIGHT)));

		cavernAtlas.load();

		final BitmapTextureAtlas buttonsAtlas = new BitmapTextureAtlas(getTextureManager(),
							mQuality.normalizeTexDim(GameResourcesManager.MENU_BUTTONS_TEXTURE_WIDTH),
							mQuality.normalizeTexDim(GameResourcesManager.MENU_BUTTONS_TEXTURE_HEIGHT) * 2, //two times the textures, two times the height must be!
							TextureOptions.BILINEAR_PREMULTIPLYALPHA);

		mResourcesManager.putTextureRegion(AssetsID.PlayButtonTextureRegion,
				BitmapTextureAtlasTextureRegionFactory.createFromAsset(buttonsAtlas, getAssets(), mQuality.ASSETS_QUALITY.assetResolvedPath("gfx/$Q/menu/play.png", "$Q"), 0, 0));

		mResourcesManager.putTextureRegion(AssetsID.OptionsButtonTextureRegion,
				BitmapTextureAtlasTextureRegionFactory.createFromAsset(buttonsAtlas, getAssets(), mQuality.ASSETS_QUALITY.assetResolvedPath("gfx/$Q/menu/options.png", "$Q"), 0, mQuality.normalizeTexDim(GameResourcesManager.MENU_BUTTONS_TEXTURE_HEIGHT)));

		buttonsAtlas.load();

		final BitmapTextureAtlas smallerTexturesAtlas = new BitmapTextureAtlas(getTextureManager(),
				mQuality.normalizeTexDim(Math.max(GameResourcesManager.PLAYER_TEXTURE_WIDTH, GameResourcesManager.LIGHTBULB_TEXTURE_WIDTH)),
				mQuality.normalizeTexDim(GameResourcesManager.PLAYER_TEXTURE_HEIGHT + GameResourcesManager.LIGHTBULB_TEXTURE_HEIGHT),
				TextureOptions.BILINEAR_PREMULTIPLYALPHA);

		mResourcesManager.putTextureRegion(AssetsID.BasePlayerTextureRegion,
				PixelPerfectTextureRegionFactory.createFromAsset(smallerTexturesAtlas, getAssets(), mQuality.ASSETS_QUALITY.assetResolvedPath("gfx/$Q/player.png", "$Q"), 0, 0, 0));

		mResourcesManager.putTextureRegion(AssetsID.LightBulbTextureRegion,
				PixelPerfectTextureRegionFactory.createFromAsset(smallerTexturesAtlas, getAssets(), mQuality.ASSETS_QUALITY.assetResolvedPath("gfx/$Q/perks/lightbulb.png", "$Q"), 0, mQuality.normalizeTexDim(GameResourcesManager.PLAYER_TEXTURE_HEIGHT), 0));

		smallerTexturesAtlas.load();

		String[][] replaceString = new String[][]{ShapesShaderProgramCostants.DEFINE_OPTIONS,
				new String[]{Integer.toString(mQuality.QUALITY_CODE), Float.toString(mQuality.WIDTH), Float.toString(mQuality.HEIGHT)}};

		mResourcesManager.putShader(AssetsID.CavernShader,
				new CavernShaderProgram(new FileShaderSource("shader/cavern/cavernvert.vert", this.getAssets(), replaceString),
										new FileShaderSource("shader/cavern/cavernfrag.frag", this.getAssets(), replaceString)));

		mResourcesManager.putShader(AssetsID.ShadowShader,
				new ShadowShaderProgram(new FileShaderSource("shader/shadow/shadow.vert", this.getAssets(), replaceString),
										new FileShaderSource("shader/shadow/shadow.frag", this.getAssets(), replaceString)));

		this.getShaderProgramManager().loadShaderProgram(mResourcesManager.getShader(AssetsID.CavernShader));
		this.getShaderProgramManager().loadShaderProgram(mResourcesManager.getShader(AssetsID.ShadowShader));

		pOnCreateResourcesCallback.onCreateResourcesFinished();
	}

	@Override
	public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback)
			throws IOException {
		getEngine().registerUpdateHandler(new FPSLogger());

		//TODO modify AndEngine to expose the initial allocation
		//TouchEvent.batchAllocatePoolItems(60);
		mSceneManager.setCurrent(SceneType.INTRO);
		pOnCreateSceneCallback.onCreateSceneFinished(mSceneManager.getCurrent());
	}

	@Override
	public void onPopulateScene(Scene pScene, OnPopulateSceneCallback pOnPopulateSceneCallback) throws IOException {
		pOnPopulateSceneCallback.onPopulateSceneFinished();
	}
}
