package com.makersf.games.nyanfly.shapes.shader;

import java.util.List;

import org.andengine.opengl.shader.ShaderProgram;
import org.andengine.opengl.util.GLState;

import com.makersf.games.nyanfly.assets.GRAPHIC_QUALITY;
import com.makersf.games.nyanfly.perks.Light;
import com.makersf.games.nyanfly.perks.LightManager;
import com.makersf.games.nyanfly.player.PlayerComponent;
import com.makersf.games.nyanfly.shapes.mesh.BindCallback;

public class ShadowBinder implements BindCallback {

	private static final int MAX_LIGHTS = 16;
	private static final float[] lights_x = new float[MAX_LIGHTS];
	private static final float[] lights_y = new float[MAX_LIGHTS];
	private static final float[] lights_distance = new float[MAX_LIGHTS];
	private static final float transition_shadow_percent_of_distance = 0.25f;

	private final PlayerComponent mPlayer;
	private final LightManager mLightManager;
	private final GRAPHIC_QUALITY mQuality;

	public ShadowBinder(PlayerComponent pPlayer, LightManager pLightManager, GRAPHIC_QUALITY pQuality) {
		mPlayer = pPlayer;
		mLightManager = pLightManager;
		mQuality = pQuality;
		// -1 because the first one is used for the player
		if(mLightManager.getMaxLights() > MAX_LIGHTS - 1) {
			throw new IllegalArgumentException("The LightManager max lights are more than the allowed ones: " + MAX_LIGHTS + ". " + mLightManager);
		}
	}

	@Override
	public void onBind(GLState pGLState, ShaderProgram pShaderProgram) {
		lights_x[0] = mPlayer.getSprite().getX();
		lights_y[0] = mPlayer.getSprite().getY();
		lights_distance[0] = mPlayer.getLightComponent().getLightIntensity() * mQuality.WIDTH;
		List<Light> lights = mLightManager.getLightInformations();
		for(int i = 0; i < lights.size() && i < MAX_LIGHTS - 1; i++) {
			Light l = lights.get(i);
			lights_x[i + 1] = l.x * mQuality.WIDTH;
			lights_y[i + 1] = l.y * mQuality.HEIGHT;
			lights_distance[i + 1] = l.intensity * mQuality.WIDTH;
		}
		for(int i = MAX_LIGHTS - 2; i >= lights.size() && i >= 0; i--) {
			lights_x[i + 1] = 0.f;
			lights_y[i + 1] = 0.f;
			lights_distance[i + 1] = 0.f;
		}
		pShaderProgram.setUniformOptional(ShapesShaderProgramCostants.UNIFORM_SHADOW_LIGHTS_X, lights_x);
		pShaderProgram.setUniformOptional(ShapesShaderProgramCostants.UNIFORM_SHADOW_LIGHTS_Y, lights_y);
		pShaderProgram.setUniformOptional(ShapesShaderProgramCostants.UNIFORM_SHADOW_LIGHTS_DISTANCE, lights_distance);
		pShaderProgram.setUniformOptional(ShapesShaderProgramCostants.UNIFORM_SHADOW_TRANSITION_PERCENT, transition_shadow_percent_of_distance);
	}

}
