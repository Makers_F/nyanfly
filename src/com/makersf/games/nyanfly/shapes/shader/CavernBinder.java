package com.makersf.games.nyanfly.shapes.shader;

import org.andengine.opengl.shader.ShaderProgram;
import org.andengine.opengl.util.GLState;

import com.makersf.games.nyanfly.assets.GRAPHIC_QUALITY;
import com.makersf.games.nyanfly.assets.GameResourcesManager;
import com.makersf.games.nyanfly.cavern.CavernProvider;
import com.makersf.games.nyanfly.shapes.mesh.BindCallback;

public class CavernBinder implements BindCallback {
	private static final float[] temp_matrix = new float[16];

	private final CavernProvider mCavernProvider;
	private final GRAPHIC_QUALITY mQuality;
	private final boolean mUpper;

	public CavernBinder(CavernProvider pCavernProvider, GRAPHIC_QUALITY pQuality, boolean pUpper) {
		mCavernProvider = pCavernProvider;
		mQuality = pQuality;
		mUpper = pUpper;
	}

	@Override
	public void onBind(GLState pGLState, ShaderProgram pShaderProgram) {
		pShaderProgram.setUniformOptional(ShapesShaderProgramCostants.UNIFORM_UNALIGNED_OFFSET, mCavernProvider.getUnalignedOffset() * mQuality.WIDTH);
		pShaderProgram.setUniformOptional(ShapesShaderProgramCostants.UNIFORM_ALIGNED_OFFSET, (mCavernProvider.getAlignedSpace() * mQuality.WIDTH) % mQuality.normalizeTexDim(GameResourcesManager.CAVERN_TEXTURE_WIDTH));
		if(mUpper) {
			pShaderProgram.setUniformOptional(ShapesShaderProgramCostants.UNIFORM_SPECIAL_COORDINATE_POSITION, mCavernProvider.getUpperPointsMatrix(temp_matrix, mQuality.WIDTH, mQuality.HEIGHT));
		} else {
			pShaderProgram.setUniformOptional(ShapesShaderProgramCostants.UNIFORM_SPECIAL_COORDINATE_POSITION, mCavernProvider.getLowerPointsMatrix(temp_matrix, mQuality.WIDTH, mQuality.HEIGHT));
		}
	}
}
