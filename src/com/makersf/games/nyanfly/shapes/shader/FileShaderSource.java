package com.makersf.games.nyanfly.shapes.shader;

import java.io.IOException;

import it.francesco.utils.string.StringUtils;
import it.francesco.utils.string.replace.StringReplace;

import org.andengine.opengl.shader.source.IShaderSource;
import org.andengine.opengl.util.GLState;

import android.content.res.AssetManager;

public class FileShaderSource implements IShaderSource {

	private final String mSource;

	public FileShaderSource(String pShaderPath, AssetManager pAssetManager) throws IOException {
		mSource = StringUtils.convertSteamToString(pAssetManager.open(pShaderPath));
	}

	/**
	 *
	 * @param pShaderPath
	 * @param pAssetManager
	 * @param pReplaceString pReplaceString[0] contains the strings to be replaced, pReplaceString[1] contains the replacing strings 
	 * @throws IOException
	 */
	public FileShaderSource(String pShaderPath, AssetManager pAssetManager, String[][] pReplaceString) throws IOException {
		mSource = StringReplace.replaceWithArray(StringUtils.convertSteamToString(pAssetManager.open(pShaderPath)), pReplaceString[0], pReplaceString[1]);
	}

	@Override
	public String getShaderSource(GLState pGLState) {
		return mSource;
	}

}
