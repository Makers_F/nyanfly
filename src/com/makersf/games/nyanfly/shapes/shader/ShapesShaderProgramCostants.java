package com.makersf.games.nyanfly.shapes.shader;

public class ShapesShaderProgramCostants {

	public static final String[] DEFINE_OPTIONS = new String[] {
		"QUALITY_CODE", "SCREEN_WIDTH", "SCREEN_HEIGHT"
	};

	public static final String UNIFORM_SPECIAL_COORDINATE_POSITION = "u_specialCoordinatesPosition";
	public static final String UNIFORM_TEXTURE_BOUNDS = "u_textureBounds";
	public static final String UNIFORM_UNALIGNED_OFFSET = "u_unalignedOffset";
	public static final String UNIFORM_ALIGNED_OFFSET = "u_alignedOffset";

	public static final String UNIFORM_SHADOW_LIGHTS_X = "u_shadow_lights_x";
	public static final String UNIFORM_SHADOW_LIGHTS_Y = "u_shadow_lights_y";
	public static final String UNIFORM_SHADOW_LIGHTS_DISTANCE = "u_shadow_lights_d";
	public static final String UNIFORM_SHADOW_TRANSITION_PERCENT = "u_shadow_transition_percent";
}
