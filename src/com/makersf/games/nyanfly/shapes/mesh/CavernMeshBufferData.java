package com.makersf.games.nyanfly.shapes.mesh;

public class CavernMeshBufferData {

	public static float[] getBufferData(final int pScreenWidth, final int pScreenHeigh, final int pVerticesPerScreen) {

		//+2 = 2 additional steps to add 2 vertices out of screen on the right
		//+1 = round to excess
		final int step_number = pVerticesPerScreen + 2;

		final int vertexCount = 2 * (step_number);// + 2;
		//4 attributes * ( 2 vertices * step_number + 2 duplicated vertices)
		float[] bufferData = new float[CavernBoundMesh.VERTEX_SIZE * vertexCount];
		final float step = pScreenWidth / pVerticesPerScreen;
/*
		//duplicate the first vertice in order to prevent glitches
		bufferData[CavernBoundMesh.VERTEX_INDEX_X] = 0;
		bufferData[CavernBoundMesh.VERTEX_INDEX_Y] = 0;
*/
		for(int i = 0; i < vertexCount; i+=2) {
			bufferData[i * CavernBoundMesh.VERTEX_SIZE + CavernBoundMesh.VERTEX_INDEX_X] = i * step / 2;
			bufferData[i * CavernBoundMesh.VERTEX_SIZE + CavernBoundMesh.VERTEX_INDEX_Y] = 0;

			bufferData[(i + 1) * CavernBoundMesh.VERTEX_SIZE + CavernBoundMesh.VERTEX_INDEX_X] = i * step / 2;
			bufferData[(i + 1) * CavernBoundMesh.VERTEX_SIZE + CavernBoundMesh.VERTEX_INDEX_Y] = pScreenHeigh / 3;
		}
/*
		//duplicate the last vertice in order to prevent glitches
		bufferData[(vertexCount - 1) * CavernBoundMesh.VERTEX_SIZE + CavernBoundMesh.VERTEX_INDEX_X] = step_number * pStep;
		bufferData[(vertexCount - 1) * CavernBoundMesh.VERTEX_SIZE + CavernBoundMesh.VERTEX_INDEX_Y] = pScreenHeigh / 3;
*/
		return bufferData;
	}

	public static short[] getIndexData(final int pVBOlenght) {
		final int trianglesNumber = (pVBOlenght - 2 );
		short[] indexData = new short[trianglesNumber * 3];

		boolean flip = false;
		for(int i = 0, n = 0; i < pVBOlenght - 2; i++, n+=3) {
			if(!flip) {
				indexData[n] = (short) (i);
				indexData[n + 1] = (short) (i + 1);
			} else {
				indexData[n] = (short) (i + 1);
				indexData[n + 1] = (short) (i);
			}
			indexData[n + 2] = (short) (i + 2);
			flip = !flip;
		}
		return indexData;
	}
}