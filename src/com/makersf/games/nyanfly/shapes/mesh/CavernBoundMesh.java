package com.makersf.games.nyanfly.shapes.mesh;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.IEntity;
import org.andengine.entity.primitive.DrawMode;
import org.andengine.entity.primitive.Line;
import org.andengine.entity.primitive.Mesh;
import org.andengine.entity.primitive.vbo.IMeshVertexBufferObject;
import org.andengine.entity.shape.Shape;
import org.andengine.opengl.shader.ShaderProgram;
import org.andengine.opengl.shader.constants.ShaderProgramConstants;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.util.GLState;
import org.andengine.opengl.vbo.DrawType;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.opengl.vbo.attribute.VertexBufferObjectAttributes;
import org.andengine.opengl.vbo.attribute.VertexBufferObjectAttributesBuilder;
import org.andengine.util.exception.MethodNotSupportedException;


import android.opengl.GLES20;

public class CavernBoundMesh extends Shape {

	// ===========================================================
	// Constants
	// ===========================================================

	public static final int VERTEX_INDEX_X = 0;
	public static final int VERTEX_INDEX_Y = CavernBoundMesh.VERTEX_INDEX_X + 1;

	public static final int VERTEX_SIZE = 2;

	public static final VertexBufferObjectAttributes VERTEXBUFFEROBJECTATTRIBUTES_DEFAULT = new VertexBufferObjectAttributesBuilder(1)
		.add(ShaderProgramConstants.ATTRIBUTE_POSITION_LOCATION, ShaderProgramConstants.ATTRIBUTE_POSITION, 2, GLES20.GL_FLOAT, false)
		.build();

	// ===========================================================
	// Fields
	// ===========================================================

	protected ITextureRegion mTextureRegion;
	protected boolean IsFlippedVertical;
	
	protected final HighPerformanceCaveMeshVertexBufferObject mMeshVertexBufferObject;
	private int mVertexCountToDraw;
	private int mDrawMode;
	private BindCallback mBindCallback;

	// ===========================================================
	// Constructors
	// ===========================================================

	public CavernBoundMesh(final float pX, final float pY, final ITextureRegion pTextureRegion, final boolean pIsFlippedVertical, final float[] pBufferData, final int pVertexCount, final DrawMode pDrawMode, final ShaderProgram pShaderProgram, final VertexBufferObjectManager pVertexBufferObjectManager) {
		this(pX, pY, pTextureRegion, pIsFlippedVertical, pBufferData, pVertexCount, pDrawMode, pShaderProgram, pVertexBufferObjectManager, DrawType.STATIC);
	}

	public CavernBoundMesh(final float pX, final float pY, final ITextureRegion pTextureRegion, final boolean pIsFlippedVertical, final float[] pBufferData, final int pVertexCount, final DrawMode pDrawMode, final ShaderProgram pShaderProgram, final VertexBufferObjectManager pVertexBufferObjectManager, final DrawType pDrawType) {
		this(pX, pY, pTextureRegion, pIsFlippedVertical, pVertexCount , pDrawMode, pShaderProgram, new HighPerformanceCaveMeshVertexBufferObject(pVertexBufferObjectManager, pBufferData, pVertexCount, pDrawType, true, CavernBoundMesh.VERTEXBUFFEROBJECTATTRIBUTES_DEFAULT));
	}
	
	public CavernBoundMesh(final float pX, final float pY, final ITextureRegion pTextureRegion, final boolean pIsFlippedVertical, final int pVertexCount, final DrawMode pDrawMode, final ShaderProgram pShaderProgram, final HighPerformanceCaveMeshVertexBufferObject pMeshVertexBufferObject) {
		super(pX, pY, pShaderProgram);

		this.mDrawMode = pDrawMode.getDrawMode();
		this.mMeshVertexBufferObject = pMeshVertexBufferObject;
		this.mVertexCountToDraw = pVertexCount;
		this.mTextureRegion = pTextureRegion;
		this.IsFlippedVertical = pIsFlippedVertical;

		this.mMeshVertexBufferObject.onUpdateTextureCoordinates(this);
		this.mMeshVertexBufferObject.setDirtyOnHardware();

		this.setBlendingEnabled(true);
	}

	// ===========================================================
	// Getter & Setter
	// ===========================================================

	public void setBindCallback(final BindCallback pBindCallback) {
		mBindCallback = pBindCallback;
	}

	public float[] getBufferData() {
		return this.mMeshVertexBufferObject.getBufferData();
	}

	public ITextureRegion getTextureRegion() {
		return this.mTextureRegion;
	}

	public void setTextureRegion(final ITextureRegion pTextureRegion) {
		mTextureRegion = pTextureRegion;
		mMeshVertexBufferObject.onUpdateTextureCoordinates(this);
	}

	public boolean isFlippedVertical() {
		return this.IsFlippedVertical;
	}

	public void setFlippedVertical(final boolean pFlippedVertical) {
		if(pFlippedVertical != IsFlippedVertical) {
			IsFlippedVertical = pFlippedVertical;
			mMeshVertexBufferObject.onUpdateTextureCoordinates(this);
		}
	}

	public void setVertexCountToDraw(final int pVertexCountToDraw) {
		this.mVertexCountToDraw = pVertexCountToDraw;
	}

	public void setDrawMode(final DrawMode pDrawMode) {
		this.mDrawMode = pDrawMode.mDrawMode;
	}

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================

	@Override
	public IMeshVertexBufferObject getVertexBufferObject() {
		return this.mMeshVertexBufferObject;
	}

	@Override
	protected void onManagedUpdate(float pSecondsElapsed) {
		super.onManagedUpdate(pSecondsElapsed);
	}

	@Override
	protected void preDraw(final GLState pGLState, final Camera pCamera) {
		super.preDraw(pGLState, pCamera);

		this.mTextureRegion.getTexture().bind(pGLState);
		
		this.mMeshVertexBufferObject.bind(pGLState, this.mShaderProgram);

		mBindCallback.onBind(pGLState, this.mShaderProgram);
	}

	@Override
	protected void draw(final GLState pGLState, final Camera pCamera) {
		this.mMeshVertexBufferObject.draw(this.mDrawMode, this.mVertexCountToDraw);
	}

	@Override
	protected void postDraw(final GLState pGLState, final Camera pCamera) {
		this.mMeshVertexBufferObject.unbind(pGLState, this.mShaderProgram);

		super.postDraw(pGLState, pCamera);
	}

	@Override
	protected void onUpdateColor() {
		throw new MethodNotSupportedException();
	}

	@Override
	protected void onUpdateVertices() {
		/* The HighPerformanceMeshVertexBufferObject leave to the caller to do all the modifications. 
		 * Since i do not need to make any update to the VBO, i just leave the method empty.*/
		
		//this.mMeshVertexBufferObject.setDirtyOnHardware(); /* Not needed since i didn't change anything*/
	}

	@Override
	@Deprecated
	public boolean contains(final float pX, final float pY) {
		throw new MethodNotSupportedException();
	}

	@Override
	public boolean collidesWith(final IEntity pOtherEntity) {
		if(pOtherEntity instanceof Mesh) {
			// TODO
			return false;
		} else if(pOtherEntity instanceof Line) {
			// TODO
			return false;
		} else {
			return false;
		}
	}

	// ===========================================================
	// Methods
	// ===========================================================

	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================

}
