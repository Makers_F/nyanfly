package com.makersf.games.nyanfly.shapes.mesh;

import org.andengine.opengl.shader.ShaderProgram;
import org.andengine.opengl.util.GLState;

public interface BindCallback {
	
	public void onBind(final GLState pGLState, final ShaderProgram pShaderProgram);
}