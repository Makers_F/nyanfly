package com.makersf.games.nyanfly.shapes.mesh;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.opengl.shader.ShaderProgram;
import org.andengine.opengl.util.GLState;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.color.Color;

public class ShadowMesh extends Rectangle {

	private BindCallback mBindCallback;

	public ShadowMesh(float pX, float pY, float pWidth, float pHeight, ShaderProgram pShaderProgram, VertexBufferObjectManager pVertexBufferObjectManager) {
		super(pX, pY, pWidth, pHeight, pVertexBufferObjectManager);
		setShaderProgram(pShaderProgram);
		setColor(Color.BLACK);
	}

	public void setBindCallback(final BindCallback pBindCallback) {
		mBindCallback = pBindCallback;
	}

	@Override
	protected void preDraw(GLState pGLState, Camera pCamera) {
		super.preDraw(pGLState, pCamera);

		mBindCallback.onBind(pGLState, getShaderProgram());
	}
}
