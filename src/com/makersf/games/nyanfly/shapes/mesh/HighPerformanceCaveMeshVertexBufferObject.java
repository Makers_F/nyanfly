package com.makersf.games.nyanfly.shapes.mesh;

import org.andengine.entity.primitive.vbo.HighPerformanceMeshVertexBufferObject;
import org.andengine.opengl.shader.ShaderProgram;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.util.GLState;
import org.andengine.opengl.vbo.DrawType;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.opengl.vbo.attribute.VertexBufferObjectAttributes;

import com.makersf.games.nyanfly.shapes.shader.ShapesShaderProgramCostants;

public class HighPerformanceCaveMeshVertexBufferObject extends
		HighPerformanceMeshVertexBufferObject{

	private static final int TEXTURE_X = 0;
	private static final int TEXTURE_Y = TEXTURE_X + 1;
	private static final int TEXTURE_WIDTH = TEXTURE_Y + 1;
	private static final int TEXTURE_HEIGHT = TEXTURE_WIDTH + 1;

	private final float[] mTextureInformations = new float[4];

	public HighPerformanceCaveMeshVertexBufferObject(final VertexBufferObjectManager pVertexBufferObjectManager, final float[] pBufferData, final int pVertexCount, final DrawType pDrawType, final boolean pAutoDispose, final VertexBufferObjectAttributes pVertexBufferObjectAttributes) {
		super(pVertexBufferObjectManager, pBufferData, pVertexCount, pDrawType, pAutoDispose, pVertexBufferObjectAttributes);
	}

	public void onUpdateTextureCoordinates(final CavernBoundMesh pCavernBoundMesh) {
		final ITextureRegion textureRegion = pCavernBoundMesh.getTextureRegion();

		final float u;
		final float v;
		final float u2;
		final float v2;

		if(pCavernBoundMesh.isFlippedVertical()) {
			u = textureRegion.getU();
			u2 = textureRegion.getU2();
			v = textureRegion.getV2();
			v2 = textureRegion.getV();
		} else {
			u = textureRegion.getU();
			u2 = textureRegion.getU2();
			v = textureRegion.getV();
			v2 = textureRegion.getV2();
		}


		if(textureRegion.isRotated()) {
			mTextureInformations[TEXTURE_X] = v;
			mTextureInformations[TEXTURE_Y] = u;
			mTextureInformations[TEXTURE_WIDTH] = v2 - v;
			mTextureInformations[TEXTURE_HEIGHT] = u2 - u;

		} else {
			mTextureInformations[TEXTURE_X] = u;
			mTextureInformations[TEXTURE_Y] = v;
			mTextureInformations[TEXTURE_WIDTH] = u2 - u;
			mTextureInformations[TEXTURE_HEIGHT] = v2 - v;
		}
	}

	@Override
	public void bind(GLState pGLState, ShaderProgram pShaderProgram) {
		super.bind(pGLState, pShaderProgram);
		pShaderProgram.setUniformOptional(ShapesShaderProgramCostants.UNIFORM_TEXTURE_BOUNDS, mTextureInformations[TEXTURE_X], mTextureInformations[TEXTURE_Y], mTextureInformations[TEXTURE_WIDTH], mTextureInformations[TEXTURE_HEIGHT]);
	}
}
