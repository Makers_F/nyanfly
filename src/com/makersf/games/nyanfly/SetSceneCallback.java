package com.makersf.games.nyanfly;

import org.andengine.engine.Engine;
import org.andengine.entity.scene.Scene;

public class SetSceneCallback {

	private final Engine mEngine;

	public SetSceneCallback(Engine pEngine) {
		mEngine = pEngine;
	}

	public void setScene(Scene pScene) {
		mEngine.setScene(pScene);
	}
}
